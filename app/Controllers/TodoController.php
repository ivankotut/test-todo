<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 02.09.17
 * Time: 16:47
 */

namespace App\Controllers;

use App\Models\Todo;
use \Eventviva\ImageResize;

class TodoController extends Controller
{

    private $errors = [];

    const PATH = __DIR__ . '/../../public/uploads/';

    public function store()
    {
        if ($this->validate($_POST['Todo'])){
            $this->render('index', ['errors' => $this->errors]);
            return false;
        }

        $photo_name = md5($_FILES['Todo']['name']['photo'] . time()) . '.' . end(explode(".", $_FILES['Todo']['name']['photo']));
        $image = new ImageResize($_FILES['Todo']['tmp_name']['photo']);
        if($image->getSourceWidth() > 320 or $image->getSourceHeight() > 240){
            $image->resize(320, 240, $allow_enlarge = True);
        }
        $image->save(self::PATH . $photo_name);


        $_POST['Todo']['photo'] = $photo_name;

        Todo::create($_POST['Todo']);

        return redirect('/');
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        if(!$this->auth){
            return redirect('/');
        }

        Todo::destroy($id);
        return redirect('/');
    }


    /**
     * @param $id
     */
    public function edit($id){
        if(!$this->auth){
            return redirect('/');
        }

        $todo = Todo::find($id);

        $this->render('edit', compact('todo'));
    }

    /**
     * @param $id
     * @return bool|void
     */
    public function save($id)
    {
        $todo = Todo::find($id);

        if(!$this->auth){
            return redirect('/');
        }

        if ($this->validate($_POST['Todo'])){
            $this->render('edit', ['errors' => $this->errors]);
            return false;
        }

        if($_FILES['Todo']['name']['photo'] != ''){
            unlink(self::PATH . $todo->photo);
            $photo_name = md5($_FILES['Todo']['name']['photo'] . time()) . '.' . end(explode(".", $_FILES['Todo']['name']['photo']));
            $image = new ImageResize($_FILES['Todo']['tmp_name']['photo']);
            if($image->getSourceWidth() > 320 or $image->getSourceHeight() > 240){
                $image->resize(320, 240, $allow_enlarge = True);
            }
            $image->save(self::PATH . $photo_name);
            $_POST['Todo']['photo'] = $photo_name;
        }

        if(!array_key_exists('checked',$_POST['Todo'])){
            $_POST['Todo']['checked'] = 0;
        }
        $todo->fill($_POST['Todo'])->save();
        return redirect('/todo/edit/' . $todo->id);

    }

    /**
     * @param $data
     * @return bool
     */
    private function validate($data)
    {
        $flagErrors = false;
        if($data['user_name'] == ''){
            $this->errors['user_name'] = 'The name field is required.';
            $flagErrors = true;
        }

        if($data['email'] == '' or
            !preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", $data['email'])){
            $this->errors['email'] = 'The email field is required.';
            $flagErrors = true;
        }

        if($data['text'] == ''){
            $this->errors['text'] = 'The text field is required.';
            $flagErrors = true;
        }


        if($_FILES['Todo']['name']['photo'] == '' and @$data['photo'] == ''){
            $this->errors['photo'] = 'The photo field is required.';
            $flagErrors = true;
        }

        return $flagErrors;
    }
}