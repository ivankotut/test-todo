<?php

namespace App\Controllers;

use App\Models\Todo;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends Controller
{
    private $take = 3;
    /**
     * todo list page
     */
    public function index()
    {
        $allCount = Todo::count();
        $currentPage = @$_GET['page'] ?: 1;

        $todos = Todo::skip($this->getSkip($currentPage))->take($this->take);
        $sort = false;
        if(isset($_GET['sort'])){
            $key = key($_GET['sort']);
            $todos->orderBy($key, $_GET['sort'][$key]);
            $sort = '&sort['. $key .']=' . $_GET['sort'][$key];
        }

        $todos = $todos->get();
        $pages = pages_array($allCount);

        $auth = $this->auth;

        $this->render('index', compact('todos', 'pages', 'currentPage', 'sort', 'auth'));
    }

    /**
     *
     */
    public function error404()
    {
        header("HTTP/1.0 404 Not Found");
        $this->render('404');
    }

    /**
     * login admin
     */
    public function login()
    {
        $this->render('login');
    }

    /**
     *
     */
    public function enter()
    {
        $login = 'admin';
        $password = '123';

        if ( isset($_POST['login']) and isset($_POST['password'])){
            if($_POST['login'] == $login and $_POST['password'] == $password){
                $_SESSION['auth'] = true;
                return redirect('/');
            }
        }

        return redirect('/login');
    }

    public function logout()
    {
        $_SESSION['auth'] = false;
        return redirect('/');
    }

    /**
     * @param $current
     * @return mixed
     */
    private function getSkip($current)
    {
        return ($current - 1) * $this->take;
    }
}