<?php

namespace App\Controllers;

/**
 * Class Controller
 * @package App\Controllers
 */
class Controller
{
    /**
     * @var bool
     */
    protected $auth = false;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        if ( isset($_SESSION['auth'])){
            if($_SESSION['auth'] === true){
                $this->auth = true;
            }
        }
    }

    /**
     * @var
     */
    private static $twig;

    /**
     * @param $twig
     */
    public static function initTwig($twig)
    {
        self::$twig = $twig;
    }

    /**
     * @param $template
     * @param array $params
     */
    protected function render($template, $params = [])
    {
        echo self::$twig->render($template . ".html.twig", $params);
    }
}