<?php
session_start();

require_once __DIR__ . '/../vendor/autoload.php';

use \Routing\Router;
use \Routing\MatchedRoute;
use \App\Controllers\Controller;

/**
 * loaded twig
 */
$loader = new Twig_Loader_Filesystem(__DIR__ . '/../app/view');
$twig = new Twig_Environment($loader, array(
//    'cache' => __DIR__ . '/../cache', //TODO
));
Controller::initTwig($twig);

try {

    $router = Router::getInstance(GET_HTTP_HOST());

    $route = $router->match(GET_METHOD(), GET_PATH_INFO());

    if (null == $route) {
        $route = new MatchedRoute('HomeController:error404');
    }

    list($class, $action) = explode(':', $route->getController(), 2);

    $class = '\App\Controllers\\' . $class;

    call_user_func_array(array(new $class($router), $action), $route->getParameters());

} catch (Exception $e) {

    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

    echo $e->getMessage();
    echo $e->getTraceAsString();
    exit;
}