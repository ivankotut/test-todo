<?php

use \Routing\Router;

$router = Router::getInstance(GET_HTTP_HOST());

$router->add('home', '/', 'HomeController:index');

$router->add('store.todo', '/todo/store', 'TodoController:store', 'POST');
$router->add('delete.todo', '/todo/delete/(id:num)', 'TodoController:delete', 'GET');
$router->add('edit.todo', '/todo/edit/(id:num)', 'TodoController:edit', 'GET');
$router->add('save.todo', '/todo/save/(id:num)', 'TodoController:save', 'POST');


$router->add('login', '/login', 'HomeController:login');
$router->add('enter.login', '/login', 'HomeController:enter', 'POST');
$router->add('exit.login', '/logout', 'HomeController:logout');
